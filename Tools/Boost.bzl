def boostArchive(name, repository, sha256):
    native.new_http_archive(
        name = name,
        urls = ["https://github.com/boostorg/" + repository + "/archive/boost-1.67.0.tar.gz"],
        sha256 = sha256,
        strip_prefix = repository + "-boost-1.67.0",
        build_file_content = """
package(default_visibility = ["//visibility:public"])
cc_library(
    name = """ + repr(name) + """,
    hdrs = glob(["include/**/*.hpp"]),
    strip_include_prefix = "include/boost",
    include_prefix = """ + repr(name) + """,
)
        """,
    )
