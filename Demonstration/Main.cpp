#include "Runtime/Database/PostgreSQL/Connection.hpp"
#include "Runtime/Database/PostgreSQL/Result.hpp"
#include "Runtime/IO/OstreamWriter.hpp"
#include "Runtime/Sources/Database/PostgreSQL/ResultSource.hpp"
#include "Runtime/Sources/MatrixSource.hpp"
#include "Runtime/Values/Value.hpp"
#include "Runtime/Widgets/Dashboard.hpp"
#include "Runtime/Widgets/ScalarWidget.hpp"
#include "Runtime/Widgets/TableWidget.hpp"

#include <BoostMultiArray/multi_array.hpp>

#include <iostream>

using namespace Runtime;

using Database::PostgreSQL::Connection;
using Database::PostgreSQL::Result;
using IO::OstreamWriter;
using Sources::Database::PostgreSQL::ResultSource;
using Sources::MatrixSource;
using Values::Value;
using Widgets::Dashboard;
using Widgets::ScalarWidget;
using Widgets::TableWidget;

int main() {
    Connection connection("");

    Dashboard dashboard;

    dashboard.panel("Square roots", [&] (auto&& renderWidget) {
        boost::multi_array<char const*, 1> arguments;
        char const* query = R"(
            SELECT series, |/ series
            FROM generate_series(1, 100) AS series
        )";
        Result data = connection.execute(query, arguments);
        ResultSource source(data);
        TableWidget widget(source);
        renderWidget(widget);
    });

    dashboard.panel("N/A", [] (auto&& renderWidget) {
        boost::multi_array<Value, 2> data(boost::extents[0][0]);
        MatrixSource source(data);
        ScalarWidget widget(source);
        renderWidget(widget);
    });

    dashboard.panel("Single Value", [] (auto&& renderWidget) {
        boost::multi_array<Value, 2> data(boost::extents[1][1]);
        data[0][0] = Value(42);
        MatrixSource source(data);
        ScalarWidget widget(source);
        renderWidget(widget);
    });

    OstreamWriter w(std::cout);
    IO::writeString(w, "HTTP/1.1 200 OK\r\n");
    IO::writeString(w, "Content-Type: text/html\r\n");
    IO::writeString(w, "\r\n");
    dashboard.renderHTML(w);

    return 0;
}
