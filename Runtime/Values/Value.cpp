#include "Runtime/Values/Value.hpp"

#include <string>

using Runtime::IO::Writer;
using Runtime::Values::NA;
using Runtime::Values::Value;

namespace {
    class RenderHTMLVisitor {
    public:
        RenderHTMLVisitor(Writer& w)
            : w(&w) {
        }

        void operator()(NA) const {
            Runtime::IO::writeString(*w, "N/A");
        }

        void operator()(std::int64_t value) const {
            std::string textual = std::to_string(value);
            Runtime::IO::writeString(*w, textual);
        }

        void operator()(std::string_view value) const {
            for (char c : value) {
                switch (c) {
                    case '<': Runtime::IO::writeString(*w, "&lt;"); break;
                    case '&': Runtime::IO::writeString(*w, "&amp;"); break;
                    default: Runtime::IO::writeAll(*w, &c, &c + 1); break;
                }
            }
        }

    private:
        Writer* w;
    };
}

namespace Runtime::Values {
    void renderHTML(Writer& w, Value const& value) {
        RenderHTMLVisitor visitor(w);
        std::visit(visitor, value);
    }
}
