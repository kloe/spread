#pragma once

#include "Runtime/IO/Writer.hpp"

#include <cstdint>
#include <string_view>
#include <variant>

namespace Runtime::Values {
    struct NA { };

    using Value = std::variant<
        NA,
        std::int64_t,
        std::string_view
    >;

    void renderHTML(IO::Writer&, Value const&);
}
