#include "Runtime/Widgets/TableWidget.hpp"

using Runtime::IO::Writer;
using Runtime::Sources::Source;
using Runtime::Values::NA;
using Runtime::Values::Value;

namespace Runtime::Widgets {
    TableWidget::TableWidget(Source const& source)
        : source(&source) {
    }

    void TableWidget::renderHTML(Writer& w) const {
        IO::writeString(w, "<table>");
        IO::writeString(w, "<tbody>");
        for (unsigned long i = 0, n = source->rowCount(); i < n; ++i) {
            IO::writeString(w, "<tr>");
            for (unsigned long j = 0, m = source->columnCount(); j < m; ++j) {
                IO::writeString(w, "<td>");
                Value value = source->operator()(i, j);
                Values::renderHTML(w, value);
                IO::writeString(w, "</td>");
            }
            IO::writeString(w, "</tr>");
        }
        IO::writeString(w, "</tbody>");
        IO::writeString(w, "</table>");
    }
}
