#include "Runtime/Widgets/Widget.hpp"

namespace Runtime::Widgets {
    Widget::~Widget() = default;
}
