#pragma once

#include "Runtime/Sources/Source.hpp"
#include "Runtime/Widgets/Widget.hpp"

namespace Runtime::Widgets {
    class TableWidget : public Widget {
    public:
        explicit TableWidget(Sources::Source const&);
        explicit TableWidget(Sources::Source&&) = delete;

        void renderHTML(IO::Writer&) const override;

    private:
        Sources::Source const* source;
    };
}
