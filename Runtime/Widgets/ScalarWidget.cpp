#include "Runtime/Widgets/ScalarWidget.hpp"

using Runtime::IO::Writer;
using Runtime::Sources::Source;
using Runtime::Values::NA;
using Runtime::Values::Value;

namespace Runtime::Widgets {
    ScalarWidget::ScalarWidget(Source const& source)
        : source(&source) {
    }

    void ScalarWidget::renderHTML(Writer& w) const {
        if (source->rowCount() == 0 ||
            source->columnCount() == 0) {
            Values::renderHTML(w, NA());
        } else {
            Value value = source->operator()(0, 0);
            Values::renderHTML(w, value);
        }
    }
}
