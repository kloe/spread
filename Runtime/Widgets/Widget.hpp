#pragma once

#include "Runtime/IO/Writer.hpp"

namespace Runtime::Widgets {
    class Widget {
    public:
        virtual ~Widget();

        virtual void renderHTML(IO::Writer&) const = 0;
    };
}
