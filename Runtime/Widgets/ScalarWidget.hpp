#pragma once

#include "Runtime/Sources/Source.hpp"
#include "Runtime/Widgets/Widget.hpp"

namespace Runtime::Widgets {
    class ScalarWidget : public Widget {
    public:
        explicit ScalarWidget(Sources::Source const&);
        explicit ScalarWidget(Sources::Source&&) = delete;

        void renderHTML(IO::Writer&) const override;

    private:
        Sources::Source const* source;
    };
}
