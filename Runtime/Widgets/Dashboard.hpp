#pragma once

#include "Runtime/Widgets/Widget.hpp"

#include <functional>
#include <string>
#include <vector>

namespace Runtime::Widgets {
    /**
     * \brief Passed to widget providers, which call this to render widgets.
     */
    class WidgetRenderer {
    public:
        virtual ~WidgetRenderer();

        virtual void operator()(Widget const&) = 0;
    };

    /**
     * \brief Collection of panels, which are widgets with metadata.
     */
    class Dashboard {
    public:
        /**
         * \brief Add a panel to this dashboard.
         *
         * \param title The title of the panel.
         * \param widgetProvider A callback that will recieve a widget renderer
         *                       to render the widget. This form of control
         *                       flow allows for lazy computation of the
         *                       widget, and easy stack-based ownership
         *                       management.
         */
        void panel(std::string title, std::function<void(WidgetRenderer&)> widgetProvider);

        /**
         * \brief Render this dashboard as HTML.
         */
        void renderHTML(IO::Writer&) const;

    private:
        struct Panel {
            Panel(std::string, std::function<void(WidgetRenderer&)>);

            void renderHTML(IO::Writer&) const;

            std::string title;
            std::function<void(WidgetRenderer&)> widgetProvider;
        };

        std::vector<Panel> panels;
    };
}
