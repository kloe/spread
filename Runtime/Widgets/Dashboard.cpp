#include "Runtime/Widgets/Dashboard.hpp"

#include <utility>

using Runtime::IO::Writer;
using Runtime::Widgets::Widget;
using Runtime::Widgets::WidgetRenderer;

namespace {
    class HTMLWidgetRenderer : public WidgetRenderer {
    public:
        explicit HTMLWidgetRenderer(Writer& w)
            : w(&w) {
        }

        void operator()(Widget const& widget) override {
            widget.renderHTML(*w);
        }

    private:
        Writer* w;
    };
}

namespace Runtime::Widgets {
    WidgetRenderer::~WidgetRenderer() = default;

    void Dashboard::panel(std::string title, std::function<void(WidgetRenderer&)> widgetProvider) {
        panels.push_back(Panel(std::move(title), std::move(widgetProvider)));
    }

    void Dashboard::renderHTML(Writer& w) const {
        for (auto&& panel : panels) {
            panel.renderHTML(w);
        }
    }

    Dashboard::Panel::Panel(std::string title, std::function<void(WidgetRenderer&)> widgetProvider)
        : title(std::move(title))
        , widgetProvider(std::move(widgetProvider)) {
    }

    void Dashboard::Panel::renderHTML(Writer& w) const {
        IO::writeString(w, "<fieldset>");
        IO::writeString(w, "<legend>");
        // TODO: Write a function to write escaped HTML text, and use it here
        //       as well as in Values::renderHTML.
        IO::writeString(w, title);
        IO::writeString(w, "</legend>");
        HTMLWidgetRenderer widgetRenderer(w);
        widgetProvider(widgetRenderer);
        IO::writeString(w, "</fieldset>");
    }
}
