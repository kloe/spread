#pragma once

#include <string_view>

namespace Runtime::IO {
    /**
     * \brief Interface for devices to which bytes can be written.
     *
     * Similar to \link std::streambuf \endlink but does not include buffering
     * and locale-awareness in the interface, since those can be implemented
     * separately and applied using the decorator pattern instead.
     */
    class Writer {
    public:
        virtual ~Writer();

        /**
         * \brief Write a range of bytes to the device.
         *
         * The begin iterator will be advanced, hence it is an lvalue reference
         * to non-const. An implementation may not write all bytes, i.e. after
         * \link write \endlink returns the begin iterator may not equal the
         * end iterator.
         */
        virtual void write(char const*&, char const*) = 0;
    };

    /**
     * \brief Write an entire range of bytes to a device.
     *
     * Like \link Writer::write \endlink, but will loop until the begin
     * iterator equals the end iterator.
     */
    void writeAll(Writer&, char const*, char const*);

    /**
     * \brief Write a string to a device.
     *
     * Like \link writeAll \endlink, but extract the range from a string view.
     */
    void writeString(Writer&, std::string_view);
}
