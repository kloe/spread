#include "Runtime/IO/OstreamWriter.hpp"

namespace Runtime::IO {
    OstreamWriter::OstreamWriter(std::ostream& ostream)
        : ostream(&ostream) {
    }

    void OstreamWriter::write(char const*& begin, char const* end) {
        ostream->write(begin, end - begin);
        begin = end;
    }
}
