#pragma once

#include "Runtime/IO/Writer.hpp"

#include <ostream>

namespace Runtime::IO {
    /**
     * \brief \link Writer \endlink-implementing adaptor for \link std::ostream
     * \endlink.
     *
     * Use this adaptor when you need an implementation of \link Writer
     * \endlink but have an implementation of \link std::ostream \endlink. This
     * uses a non-owning reference to the \link std::ostream \endlink, and
     * calls its \link std::ostream::write \endlink non-static member function.
     */
    class OstreamWriter : public Writer {
    public:
        explicit OstreamWriter(std::ostream&);

        void write(char const*&, char const*) override;

    private:
        std::ostream* ostream;
    };
}
