#include "Runtime/IO/Writer.hpp"

namespace Runtime::IO {
    Writer::~Writer() = default;

    void writeAll(Writer& w, char const* begin, char const* end) {
        while (begin != end) {
            w.write(begin, end);
        }
    }

    void writeString(Writer& w, std::string_view string) {
        writeAll(w, string.begin(), string.end());
    }
}
