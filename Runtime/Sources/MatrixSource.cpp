#include "Runtime/Sources/MatrixSource.hpp"

using Runtime::Values::Value;

using MARef = boost::const_multi_array_ref<Value, 2>;

namespace Runtime::Sources {
    MatrixSource::MatrixSource(MARef source)
        : source(source) {
    }

    unsigned long MatrixSource::rowCount() const {
        return source.shape()[0];
    }

    unsigned long MatrixSource::columnCount() const {
        return source.shape()[1];
    }

    Value MatrixSource::operator()(unsigned long row, unsigned long column) const {
        return source[row][column];
    }
}
