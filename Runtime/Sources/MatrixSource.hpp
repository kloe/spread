#pragma once

#include "Runtime/Sources/Source.hpp"

#include "BoostMultiArray/multi_array.hpp"

namespace Runtime::Sources {
    class MatrixSource : public Source {
    public:
        explicit MatrixSource(boost::const_multi_array_ref<Values::Value, 2>);

        unsigned long rowCount() const override;
        unsigned long columnCount() const override;
        Values::Value operator()(unsigned long, unsigned long) const override;

    private:
        boost::const_multi_array_ref<Values::Value, 2> source;
    };
}
