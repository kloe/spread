#include "Runtime/Sources/Source.hpp"

namespace Runtime::Sources {
    Source::~Source() = default;
}
