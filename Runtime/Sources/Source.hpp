#pragma once

#include "Runtime/Values/Value.hpp"

namespace Runtime::Sources {
    class Source {
    public:
        virtual ~Source();

        virtual unsigned long rowCount() const = 0;
        virtual unsigned long columnCount() const = 0;
        virtual Values::Value operator()(unsigned long, unsigned long) const = 0;
    };
}
