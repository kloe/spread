#pragma once

#include "Runtime/Database/PostgreSQL/Result.hpp"
#include "Runtime/Sources/Source.hpp"

namespace Runtime::Sources::Database::PostgreSQL {
    class ResultSource : public Source {
    public:
        explicit ResultSource(Runtime::Database::PostgreSQL::Result const&);
        explicit ResultSource(Runtime::Database::PostgreSQL::Result&&) = delete;

        unsigned long rowCount() const override;
        unsigned long columnCount() const override;
        Values::Value operator()(unsigned long, unsigned long) const override;

    private:
        Runtime::Database::PostgreSQL::Result const* result;
    };
}
