#include "Runtime/Sources/Database/PostgreSQL/ResultSource.hpp"

#include <cassert>
#include <limits>

using Runtime::Database::PostgreSQL::Result;
using Runtime::Values::NA;
using Runtime::Values::Value;

namespace Runtime::Sources::Database::PostgreSQL {
    ResultSource::ResultSource(Result const& result)
        : result(&result) {
    }

    unsigned long ResultSource::rowCount() const {
        return result->rowCount();
    }

    unsigned long ResultSource::columnCount() const {
        return result->columnCount();
    }

    Value ResultSource::operator()(unsigned long row, unsigned long column) const {
        assert(row <= std::numeric_limits<int>::max());
        assert(column <= std::numeric_limits<int>::max());
        std::optional<std::string_view> textual =
            result->operator()(row, column);
        if (textual) {
            return *textual;
        } else {
            return NA();
        }
    }
}
