#pragma once

#include <libpq-fe.h>

#include <memory>
#include <optional>
#include <string_view>

namespace Runtime::Database::PostgreSQL {
    /**
     * \brief PostgreSQL command result.
     *
     * Such an object is returned when executing a command, and can be used to
     * query the shape and data from a result set.
     */
    class Result {
    public:
        /**
         * \brief The number of rows in the result.
         */
        int rowCount() const;

        /**
         * \brief The number of columns in the result.
         */
        int columnCount() const;

        /**
         * \brief The value at the given cell index in the result.
         *
         * Returns \link std::optional \endlink because values may be SQL \c
         * NULL.
         */
        std::optional<std::string_view> operator()(int, int) const;

    private:
        struct PGresultDelete {
            void operator()(::PGresult*) const;
        };

        explicit Result(std::unique_ptr<::PGresult, PGresultDelete>);

        std::unique_ptr<::PGresult, PGresultDelete> result;

        friend class Connection;
    };
}
