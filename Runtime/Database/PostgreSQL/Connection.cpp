#include "Runtime/Database/PostgreSQL/Connection.hpp"
#include "Runtime/Database/PostgreSQL/Errors.hpp"

#include <utility>

namespace Runtime::Database::PostgreSQL {
    Connection::Connection(char const* connectionString)
        : connection(::PQconnectdb(connectionString)) {
        PostgreSQL::checkConnection(connection.get());
    }

    Result Connection::execute(char const* command, boost::const_multi_array_ref<char const*, 1> arguments) {
        std::unique_ptr<::PGresult, Result::PGresultDelete> result(
            ::PQexecParams(
                /* conn */         connection.get(),
                /* command */      command,
                /* nParams */      arguments.size(),
                /* paramTypes */   nullptr,
                /* paramValues */  arguments.data(),
                /* paramLengths */ nullptr,
                /* paramFormats */ nullptr,
                /* resultFormat */ 0
            )
        );
        PostgreSQL::checkResult(connection.get(), result.get());
        return Result(std::move(result));
    }

    void Connection::PGconnDelete::operator()(::PGconn* connection) const {
        if (connection != nullptr) {
            ::PQfinish(connection);
        }
    }
}
