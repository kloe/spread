#include "Runtime/Database/PostgreSQL/Result.hpp"

#include <cassert>
#include <utility>

namespace Runtime::Database::PostgreSQL {
    int Result::rowCount() const {
        return ::PQntuples(result.get());
    }

    int Result::columnCount() const {
        return ::PQnfields(result.get());
    }

    std::optional<std::string_view> Result::operator()(int row, int column) const {
        assert(row >= 0 && row < rowCount());
        assert(column >= 0 && column <= columnCount());

        if (::PQgetisnull(result.get(), row, column)) {
            return std::nullopt;
        }

        return std::string_view(
            ::PQgetvalue(result.get(), row, column),
            ::PQgetlength(result.get(), row, column)
        );
    }

    void Result::PGresultDelete::operator()(::PGresult* result) const {
        if (result != nullptr) {
            ::PQclear(result);
        }
    }

    Result::Result(std::unique_ptr<::PGresult, PGresultDelete> result)
        : result(std::move(result)) {
    }
}
