#include "Runtime/Database/PostgreSQL/Errors.hpp"

#include <new>

namespace Runtime::Database::PostgreSQL {
    void checkConnection(::PGconn* connection) {
        if (connection == nullptr) {
            throw std::bad_alloc();
        }

        if (::PQstatus(connection) != CONNECTION_OK) {
            char const* message = ::PQerrorMessage(connection);
            throw ConnectionError(message);
        }
    }

    void checkResult(::PGconn* connection, ::PGresult* result) {
        if (result == nullptr) {
            char const* message = ::PQerrorMessage(connection);
            throw ExecuteError(message);
        }

        char const* message = ::PQresultErrorMessage(result);
        if (message[0] != '\0') {
            throw ExecuteError(message);
        }
    }
}
