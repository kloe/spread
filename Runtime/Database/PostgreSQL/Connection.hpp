#pragma once

#include "Runtime/Database/PostgreSQL/Result.hpp"

#include "BoostMultiArray/multi_array.hpp"

#include <libpq-fe.h>

#include <memory>

namespace Runtime::Database::PostgreSQL {
    /**
     * \brief PostgreSQL database connection; wrapper around \link PGconn
     * \endlink.
     *
     * The destructor closes the connection.
     */
    class Connection {
    public:
        /**
         * \brief Connect to a PostgreSQL database given a NUL-terminated
         * connection string.
         *
         * The format of the connection string is as accepted by \link
         * PQconnectdb \endlink.
         */
        explicit Connection(char const*);

        /**
         * \brief Execute a parameterized command and return its result.
         *
         * Arguments must be in the text format, and NUL-terminated. Pass \c
         * nullptr for an argument if you want to pass a SQL \c NULL value.
         */
        Result execute(char const*, boost::const_multi_array_ref<char const*, 1>);

    private:
        struct PGconnDelete {
            void operator()(::PGconn*) const;
        };

        std::unique_ptr<::PGconn, PGconnDelete> connection;
    };
}
