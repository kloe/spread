#pragma once

#include <libpq-fe.h>

#include <stdexcept>

namespace Runtime::Database::PostgreSQL {
    /**
     * \brief Base class for PostgreSQL errors.
     */
    class Error : public std::runtime_error {
    public:
        using std::runtime_error::runtime_error;
    };

    /**
     * \brief Error raised during connecting.
     */
    class ConnectionError : public Error {
    public:
        using Error::Error;
    };

    /**
     * \brief Error raised during command execution.
     */
    class ExecuteError : public Error {
    public:
        using Error::Error;
    };

    /**
     * \brief Check if connecting was successful.
     */
    void checkConnection(::PGconn*);

    /**
     * \brief Check if command execution was successful.
     */
    void checkResult(::PGconn*, ::PGresult*);
}
